# translation of noosfero.po to
# Krishnamurti Lelis Lima Vieira Nunes <krishna@colivre.coop.br>, 2007.
# noosfero - Brazilian Portuguese translation
# Copyright (C) 2007,
# Forum Brasileiro de Economia Solidaria <http://www.fbes.org.br/>
# Copyright (C) 2007,
# Ynternet.org Foundation <http://www.ynternet.org/>
# This file is distributed under the same license as noosfero itself.
# Joenio Costa <joenio@colivre.coop.br>, 2008.
#
#
msgid ""
msgstr ""
"Project-Id-Version: 1.3~rc2-1-ga15645d\n"
"PO-Revision-Date: 2019-01-08 14:07+0000\n"
"Last-Translator: Rebeca Moura <bcasamo@gmail.com>\n"
"Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/"
"plugin-breadcrumbs/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.4-dev\n"

msgid "A plugin that add a block to display breadcrumbs."
msgstr "Um plugin que adiciona um bloco que mostra caminhos de pão."

msgid "Breadcrumbs"
msgstr "Caminho de Pão"

msgid ""
"<p>Display a breadcrumb of the current content navigation.</p><p>You could "
"choose if the breadcrumb is going to appear in the cms editing or not.</p> "
"<p>There is either the option of display the profile location in the "
"breadcrumb path.</p>"
msgstr ""
"<p>Mostra um caminho de pão do conteúdo atual da navegação.</p><p>Você pode "
"escolher se o caminho de pão aparecerá na edição do cms ou não.</p> "
"<p>Existe também a opção de mostrar a localização do perfil no endereço do "
"caminho de pão.</p>"

msgid "Breadcrumb"
msgstr "Caminho de Pão"

msgid "Breadcrumbs Block"
msgstr "Bloco de Caminho de Pão"

msgid "This block displays breadcrumb trail."
msgstr "Esse bloco mostra o rastro do caminho de pão."

msgid "Upload Files"
msgstr "Enviar Arquivos"

msgid "Members"
msgstr "Membros"

msgid "Events"
msgstr "Eventos"

msgid "Profile"
msgstr "Perfil"

msgid "Show cms action"
msgstr "Mostrar cms"

msgid "Show profile"
msgstr "Mostrar perfil"

msgid "Show section name"
msgstr "Mostrar nome da seção"
